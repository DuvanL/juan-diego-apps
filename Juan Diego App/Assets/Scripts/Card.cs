﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Card", menuName = "Card")]
public class Card : ScriptableObject
{

    public string InstrumentName;

    public List<string> description = new List<string>();

    public List<Sprite> Photos;

}