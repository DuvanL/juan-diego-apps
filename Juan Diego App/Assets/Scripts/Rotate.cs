﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour
{
    public Transform target;
    float distance = 10.0f;

    float xSpeed = 250.0f;
    float ySpeed = 120.0f;

    float yMinLimit = -20f;
    float yMaxLimit = 80f;

    private float x = 0.0f;
    private float y = 0.0f;

    float xsign = 1;



    void Start()
    {
        var angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;

        //var rotation = Quaternion.Euler(y, x, 0);
        //var position = rotation * new Vector3(0.0f, -distance, 0.0f) + target.position;

        //transform.rotation = rotation;
        //transform.position = position;


    }

    void LateUpdate()
    {

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Moved && Input.touchCount == 1)
            {
                x -= xsign * touch.deltaPosition.x * xSpeed * 0.001f;

                var rotation = Quaternion.Euler(0, x, 0);
                //this.gameObject.GetComponent<Transform>().Rotate(new Vector3(0, x, 0) * Time.deltaTime);
                transform.rotation = rotation;

            }
        }
    }

}
