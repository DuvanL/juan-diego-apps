﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class CustomWindow : EditorWindow
{
    int Number;

    [MenuItem("Window/Cards creator")]
    public static void ShowWindow()
    {
        GetWindow<CustomWindow>("Cards creator");
    }

    void OnGUI()
    {
        GUILayout.Label("Number of cards to create", EditorStyles.boldLabel);

        Number = EditorGUILayout.IntField("Number", Number);

        if (GUILayout.Button("Create!"))
        {
            Create();
        }
    }

    void Create()
    {
        for (int i = 0; i < Number; i++)
        {
           ScriptableObjectUtility.CreateAsset<Card>();
        }
            
    }

}