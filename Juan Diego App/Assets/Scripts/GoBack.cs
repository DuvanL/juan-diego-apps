﻿using UnityEngine;

public class GoBack : MonoBehaviour
{

    public string sceneToGo;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Menu) || Input.GetKeyDown(KeyCode.Escape))
        {
            print("Voy para afuera");
            GameObject.Find("Canvas").GetComponent<sceneChangerClick>().irAEscena(sceneToGo);
            return;
        }
    }
}
